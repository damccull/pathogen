﻿namespace PATHogen
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.ListViewItem listViewItem1 = new System.Windows.Forms.ListViewItem("Red");
            System.Windows.Forms.ListViewItem listViewItem2 = new System.Windows.Forms.ListViewItem("Blue");
            System.Windows.Forms.ListViewItem listViewItem3 = new System.Windows.Forms.ListViewItem("Green");
            System.Windows.Forms.ListViewItem listViewItem4 = new System.Windows.Forms.ListViewItem("Yellow");
            System.Windows.Forms.ListViewItem listViewItem5 = new System.Windows.Forms.ListViewItem("Red");
            System.Windows.Forms.ListViewItem listViewItem6 = new System.Windows.Forms.ListViewItem("Blue");
            System.Windows.Forms.ListViewItem listViewItem7 = new System.Windows.Forms.ListViewItem("Green");
            System.Windows.Forms.ListViewItem listViewItem8 = new System.Windows.Forms.ListViewItem("Yellow");
            this.listViewSystemPath = new System.Windows.Forms.ListView();
            this.Path = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuFileExit = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuHelpAbout = new System.Windows.Forms.ToolStripMenuItem();
            this.listViewUserPath = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.tabControlPath = new System.Windows.Forms.TabControl();
            this.tabPageUserPath = new System.Windows.Forms.TabPage();
            this.buttonRemoveUserPath = new System.Windows.Forms.Button();
            this.buttonAddUserPath = new System.Windows.Forms.Button();
            this.tabPageSystemPath = new System.Windows.Forms.TabPage();
            this.buttonRemoveSystemPath = new System.Windows.Forms.Button();
            this.buttonAddSystemPath = new System.Windows.Forms.Button();
            this.buttonSaveSystemPath = new System.Windows.Forms.Button();
            this.buttonSaveUserPath = new System.Windows.Forms.Button();
            this.menuStrip1.SuspendLayout();
            this.tabControlPath.SuspendLayout();
            this.tabPageUserPath.SuspendLayout();
            this.tabPageSystemPath.SuspendLayout();
            this.SuspendLayout();
            // 
            // listViewSystemPath
            // 
            this.listViewSystemPath.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listViewSystemPath.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.Path});
            this.listViewSystemPath.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
            this.listViewSystemPath.Items.AddRange(new System.Windows.Forms.ListViewItem[] {
            listViewItem1,
            listViewItem2,
            listViewItem3,
            listViewItem4});
            this.listViewSystemPath.Location = new System.Drawing.Point(8, 6);
            this.listViewSystemPath.Name = "listViewSystemPath";
            this.listViewSystemPath.Size = new System.Drawing.Size(688, 318);
            this.listViewSystemPath.TabIndex = 0;
            this.listViewSystemPath.UseCompatibleStateImageBehavior = false;
            this.listViewSystemPath.View = System.Windows.Forms.View.Details;
            // 
            // Path
            // 
            this.Path.Text = "Paths";
            this.Path.Width = 469;
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.SystemColors.Control;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(712, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MenuFileExit});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "&File";
            // 
            // MenuFileExit
            // 
            this.MenuFileExit.Name = "MenuFileExit";
            this.MenuFileExit.Size = new System.Drawing.Size(92, 22);
            this.MenuFileExit.Text = "E&xit";
            this.MenuFileExit.Click += new System.EventHandler(this.ToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MenuHelpAbout});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "&Help";
            // 
            // MenuHelpAbout
            // 
            this.MenuHelpAbout.Name = "MenuHelpAbout";
            this.MenuHelpAbout.Size = new System.Drawing.Size(152, 22);
            this.MenuHelpAbout.Text = "&About";
            this.MenuHelpAbout.Click += new System.EventHandler(this.ToolStripMenuItem_Click);
            // 
            // listViewUserPath
            // 
            this.listViewUserPath.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listViewUserPath.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1});
            this.listViewUserPath.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
            this.listViewUserPath.Items.AddRange(new System.Windows.Forms.ListViewItem[] {
            listViewItem5,
            listViewItem6,
            listViewItem7,
            listViewItem8});
            this.listViewUserPath.Location = new System.Drawing.Point(8, 6);
            this.listViewUserPath.Name = "listViewUserPath";
            this.listViewUserPath.Size = new System.Drawing.Size(688, 318);
            this.listViewUserPath.TabIndex = 2;
            this.listViewUserPath.UseCompatibleStateImageBehavior = false;
            this.listViewUserPath.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Paths";
            this.columnHeader1.Width = 469;
            // 
            // tabControlPath
            // 
            this.tabControlPath.Controls.Add(this.tabPageUserPath);
            this.tabControlPath.Controls.Add(this.tabPageSystemPath);
            this.tabControlPath.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlPath.Location = new System.Drawing.Point(0, 24);
            this.tabControlPath.Name = "tabControlPath";
            this.tabControlPath.SelectedIndex = 0;
            this.tabControlPath.Size = new System.Drawing.Size(712, 387);
            this.tabControlPath.TabIndex = 5;
            // 
            // tabPageUserPath
            // 
            this.tabPageUserPath.Controls.Add(this.buttonSaveUserPath);
            this.tabPageUserPath.Controls.Add(this.buttonRemoveUserPath);
            this.tabPageUserPath.Controls.Add(this.buttonAddUserPath);
            this.tabPageUserPath.Controls.Add(this.listViewUserPath);
            this.tabPageUserPath.Location = new System.Drawing.Point(4, 22);
            this.tabPageUserPath.Name = "tabPageUserPath";
            this.tabPageUserPath.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageUserPath.Size = new System.Drawing.Size(704, 361);
            this.tabPageUserPath.TabIndex = 0;
            this.tabPageUserPath.Text = "User PATH";
            this.tabPageUserPath.UseVisualStyleBackColor = true;
            // 
            // buttonRemoveUserPath
            // 
            this.buttonRemoveUserPath.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonRemoveUserPath.Location = new System.Drawing.Point(621, 330);
            this.buttonRemoveUserPath.Name = "buttonRemoveUserPath";
            this.buttonRemoveUserPath.Size = new System.Drawing.Size(75, 23);
            this.buttonRemoveUserPath.TabIndex = 4;
            this.buttonRemoveUserPath.Text = "Remove";
            this.buttonRemoveUserPath.UseVisualStyleBackColor = true;
            this.buttonRemoveUserPath.Click += new System.EventHandler(this.buttonPath_Click);
            // 
            // buttonAddUserPath
            // 
            this.buttonAddUserPath.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonAddUserPath.Location = new System.Drawing.Point(540, 330);
            this.buttonAddUserPath.Name = "buttonAddUserPath";
            this.buttonAddUserPath.Size = new System.Drawing.Size(75, 23);
            this.buttonAddUserPath.TabIndex = 3;
            this.buttonAddUserPath.Text = "Add";
            this.buttonAddUserPath.UseVisualStyleBackColor = true;
            this.buttonAddUserPath.Click += new System.EventHandler(this.buttonPath_Click);
            // 
            // tabPageSystemPath
            // 
            this.tabPageSystemPath.Controls.Add(this.buttonSaveSystemPath);
            this.tabPageSystemPath.Controls.Add(this.buttonRemoveSystemPath);
            this.tabPageSystemPath.Controls.Add(this.buttonAddSystemPath);
            this.tabPageSystemPath.Controls.Add(this.listViewSystemPath);
            this.tabPageSystemPath.Location = new System.Drawing.Point(4, 22);
            this.tabPageSystemPath.Name = "tabPageSystemPath";
            this.tabPageSystemPath.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageSystemPath.Size = new System.Drawing.Size(704, 361);
            this.tabPageSystemPath.TabIndex = 1;
            this.tabPageSystemPath.Text = "System PATH";
            this.tabPageSystemPath.UseVisualStyleBackColor = true;
            // 
            // buttonRemoveSystemPath
            // 
            this.buttonRemoveSystemPath.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonRemoveSystemPath.Location = new System.Drawing.Point(621, 330);
            this.buttonRemoveSystemPath.Name = "buttonRemoveSystemPath";
            this.buttonRemoveSystemPath.Size = new System.Drawing.Size(75, 23);
            this.buttonRemoveSystemPath.TabIndex = 2;
            this.buttonRemoveSystemPath.Text = "Remove";
            this.buttonRemoveSystemPath.UseVisualStyleBackColor = true;
            this.buttonRemoveSystemPath.Click += new System.EventHandler(this.buttonPath_Click);
            // 
            // buttonAddSystemPath
            // 
            this.buttonAddSystemPath.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonAddSystemPath.Location = new System.Drawing.Point(540, 330);
            this.buttonAddSystemPath.Name = "buttonAddSystemPath";
            this.buttonAddSystemPath.Size = new System.Drawing.Size(75, 23);
            this.buttonAddSystemPath.TabIndex = 1;
            this.buttonAddSystemPath.Text = "Add";
            this.buttonAddSystemPath.UseVisualStyleBackColor = true;
            this.buttonAddSystemPath.Click += new System.EventHandler(this.buttonPath_Click);
            // 
            // buttonSaveSystemPath
            // 
            this.buttonSaveSystemPath.Location = new System.Drawing.Point(327, 330);
            this.buttonSaveSystemPath.Name = "buttonSaveSystemPath";
            this.buttonSaveSystemPath.Size = new System.Drawing.Size(75, 23);
            this.buttonSaveSystemPath.TabIndex = 6;
            this.buttonSaveSystemPath.Text = "Save";
            this.buttonSaveSystemPath.UseVisualStyleBackColor = true;
            this.buttonSaveSystemPath.Click += new System.EventHandler(this.buttonPath_Click);
            // 
            // buttonSaveUserPath
            // 
            this.buttonSaveUserPath.Location = new System.Drawing.Point(327, 330);
            this.buttonSaveUserPath.Name = "buttonSaveUserPath";
            this.buttonSaveUserPath.Size = new System.Drawing.Size(75, 23);
            this.buttonSaveUserPath.TabIndex = 7;
            this.buttonSaveUserPath.Text = "Save";
            this.buttonSaveUserPath.UseVisualStyleBackColor = true;
            this.buttonSaveUserPath.Click += new System.EventHandler(this.buttonPath_Click);
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(712, 411);
            this.Controls.Add(this.tabControlPath);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.Name = "FormMain";
            this.Text = "PATHogen";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.tabControlPath.ResumeLayout(false);
            this.tabPageUserPath.ResumeLayout(false);
            this.tabPageSystemPath.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView listViewSystemPath;
        private System.Windows.Forms.ColumnHeader Path;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem MenuFileExit;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem MenuHelpAbout;
        private System.Windows.Forms.ListView listViewUserPath;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.TabControl tabControlPath;
        private System.Windows.Forms.TabPage tabPageUserPath;
        private System.Windows.Forms.TabPage tabPageSystemPath;
        private System.Windows.Forms.Button buttonAddSystemPath;
        private System.Windows.Forms.Button buttonRemoveUserPath;
        private System.Windows.Forms.Button buttonAddUserPath;
        private System.Windows.Forms.Button buttonRemoveSystemPath;
        private System.Windows.Forms.Button buttonSaveSystemPath;
        private System.Windows.Forms.Button buttonSaveUserPath;
    }
}

