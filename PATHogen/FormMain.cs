﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace PATHogen {
    public partial class FormMain : Form {
        public RegistryEditor RegistryEditor { get; set; }

        private string formCaption = "PATHogen";


        #region Admin Button Stuff
        [DllImport("user32")]
        public static extern UInt32 SendMessage
            (IntPtr hWnd, UInt32 msg, UInt32 wParam, UInt32 lParam);

        internal const int BCM_NORMALBUTTON = 0x1600; //Normal button
        internal const int BCM_SETSHIELD = (BCM_NORMALBUTTON + 0x000C); //Elevated button 
        #endregion

        private bool _SystemModified;
        /// <summary>
        /// Stores the modified state of the path lists.
        /// </summary>
        public bool SystemModified {
            get { return _SystemModified; }
            set {
                _SystemModified = value;
                updateCaption();
            }
        }
        
        private bool _UserModified;
        /// <summary>
        /// Stores the modified state of the path lists.
        /// </summary>
        public bool UserModified {
            get { return _UserModified; }
            set {
                _UserModified = value;
                updateCaption();
            }
        }
        

        /// <summary>
        /// Constructs the form and performs initial setup.
        /// </summary>
        public FormMain() { 
            InitializeComponent();
            //Set up a RegistryEditor object so we can do stuff
            this.RegistryEditor = new RegistryEditor();
            //Ensure Modified property is set to false
            this.SystemModified = false;

            addShieldToButton(buttonSaveSystemPath);

            //Refresh the path lists so we can see stuff in them
            refreshPathLists();
            
        }

        /// <summary>
        /// Adds a UAC shield to the button if necessary.
        /// </summary>
        /// <param name="buttonSaveSystemPath">The button to add a shield to.</param>
        private void addShieldToButton(Button button) {
            //Determine if the process is running as an admin.
            //If not, add a UAC shield.
            if (!Program.HasAdministratorPrivileges) {
                button.FlatStyle = FlatStyle.System;
                SendMessage(button.Handle, BCM_SETSHIELD, 0, 0xFFFFFFFF);
            }
        }

        private void updateCaption() {
            //When this property is set, change the name of the form to reflect the modified status
            if (UserModified || SystemModified) {
                this.Text = formCaption + "*";
            } else {
                this.Text = formCaption;
            }
        }

        private void refreshPathLists() {
            //Get the path values for the system and populate the listview
            this.listViewSystemPath.Items.Clear();
            foreach (String s in RegistryEditor.LoadPathsFromRegistry(EnvironmentVariableTarget.Machine)) {
                this.listViewSystemPath.Items.Add(s);
            }

            //Get the path values for the user and populate the listview
            this.listViewUserPath.Items.Clear();
            foreach (String s in RegistryEditor.LoadPathsFromRegistry(EnvironmentVariableTarget.User)) {
                this.listViewUserPath.Items.Add(s);
            }
        }

        /// <summary>
        /// Handles all ToolStripMenu events for this form.
        /// </summary>
        /// <param name="sender">The sending control.</param>
        /// <param name="e">Any arguments from the event.</param>
        private void ToolStripMenuItem_Click(object sender, EventArgs e) {
            ToolStripMenuItem m = (ToolStripMenuItem)sender;

            switch (m.Name) {
                case "MenuFileExit":
                    Environment.Exit(0);
                    break;
                case "MenuHelpAbout":
                    new AboutBox().ShowDialog();
                    break;
            }
        }

        /// <summary>
        /// Handles the click operations for add and remove path buttons.
        /// </summary>
        /// <param name="sender">The sending control.</param>
        /// <param name="e">Any arguments from the event.</param>
        private void buttonPath_Click(object sender, EventArgs e) {
            Button b = (Button)sender;
            string writePath;

            switch (b.Name) {
                case "buttonAddUserPath":
                    AddPath(EnvironmentVariableTarget.User);
                    break;
                case "buttonRemoveUserPath":
                    RemovePath(EnvironmentVariableTarget.User);
                    break;
                case "buttonAddSystemPath":
                    AddPath(EnvironmentVariableTarget.Machine);
                    break;
                case "buttonRemoveSystemPath":
                    RemovePath(EnvironmentVariableTarget.Machine);
                    break;
                case "buttonSaveSystemPath":
                    writePath = "";
                    //Get all the paths from the listview and build a path string
                    foreach (ListViewItem i in listViewSystemPath.Items) {
                        writePath += i.Text + ";";
                    }
                    writePath = writePath.Substring(0, writePath.Length - 1);
                    RegistryEditor.WritePathsToRegistry(EnvironmentVariableTarget.Machine,writePath);
 

                    //Refresh the list
                    refreshPathLists();
                    this.SystemModified = false;
                    break;
                case "buttonSaveUserPath":
                    writePath = "";
                    //Get all the paths from the listview and build a path string
                    foreach (ListViewItem i in listViewUserPath.Items) {
                        writePath += i.Text + ";";
                    }
                    writePath = writePath.Substring(0, writePath.Length - 1);
                    RegistryEditor.WritePathsToRegistry(EnvironmentVariableTarget.User,writePath);
 

                    //Refresh the list
                    refreshPathLists();
                    this.UserModified = false;
                    break;
                default:
                    break;
            }
        }


        /// <summary>
        /// Adds a path to either the User or System list.
        /// </summary>
        /// <param name="type">The list to add the path to.</param>
        public void AddPath(EnvironmentVariableTarget type) {
            //Display a folder dialog for user to select a folder
            FolderBrowserDialog fbd = new FolderBrowserDialog();
            //If the user clicked anything but OK, don't do anything
            if (fbd.ShowDialog() != DialogResult.OK) {
                return;
            }
            //If user clicked OK, do something depending on type of PATH
            switch (type) {
                case EnvironmentVariableTarget.User:
                    //Type is a USER path
                    listViewUserPath.Items.Add(fbd.SelectedPath);
                    this.UserModified = true;
                    break;
                case EnvironmentVariableTarget.Machine:
                    //Type is a SYSTEM path
                    listViewSystemPath.Items.Add(fbd.SelectedPath);
                    this.SystemModified = true;
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// Removes a path from either the User or System list.
        /// </summary>
        /// <param name="type">The list to remove the path from.</param>
        public void RemovePath(EnvironmentVariableTarget type) {
            //Verify the user wants to delete crap
            DialogResult result = MessageBox.Show("Are you sure you want to remove the selected items from the path?" + Environment.NewLine + "This can not be undone once saved!", "Alert!", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            //If user does anything but click YES, don't do anything
            if (result != DialogResult.Yes) {
                return;
            }
            switch (type) {
                case EnvironmentVariableTarget.User:
                    foreach (ListViewItem item in listViewUserPath.SelectedItems) {
                        listViewUserPath.Items.Remove(item);                        
                    }
                    this.UserModified = true;
                    break;
                case EnvironmentVariableTarget.Machine:
                    foreach (ListViewItem item in listViewSystemPath.SelectedItems) {
                        listViewSystemPath.Items.Remove(item);
                    }
                    this.SystemModified = true;
                    break;
                default:
                    break;
            }
        }

    }
}
