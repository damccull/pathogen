﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Principal;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PATHogen
{
    static class Program
    {
        public static bool HasAdministratorPrivileges { get; set; }


        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            //Check if current user has Administrator role.
            WindowsPrincipal principal = new WindowsPrincipal(WindowsIdentity.GetCurrent());
            Program.HasAdministratorPrivileges = principal.IsInRole(WindowsBuiltInRole.Administrator);

            //Get command line arguments
            String[] args = Environment.GetCommandLineArgs();

            if (Program.HasAdministratorPrivileges && args != null && args.Length > 0) {
                //If user has Administrator role and there are command line args...

                //set the path as an an expandable string
                Registry.LocalMachine.CreateSubKey(RegistryEditor.RegKeySystemPath).SetValue("Path", args[1] , RegistryValueKind.ExpandString);

                Environment.Exit(0);
            }

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new FormMain());
        }
    }
}
