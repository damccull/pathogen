﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace PATHogen {
    public class RegistryEditor {
        #region Properties
        /// <summary>
        /// Returns the registry path for the Local Machine environment variables.
        /// </summary>
        public static string RegKeySystemPath {
            get {
                return @"SYSTEM\CurrentControlSet\Control\Session Manager\Environment";
            }
        }
        /// <summary>
        /// Returns the registry path for the Current User environment variables.
        /// </summary>
        public static string RegKeyUserPath {
            get {
                return @"Environment";
            }
        } 
        #endregion

        /// <summary>
        /// Reads the existing PATH environment variable from the registry.
        /// </summary>
        /// <param name="type">The USER or SYSTEM path.</param>
        /// <returns>A <see cref="List"/> of paths.</returns>
        public List<String> LoadPathsFromRegistry(EnvironmentVariableTarget type) {
            string outPath = "";

            switch (type) {
                case EnvironmentVariableTarget.Machine:
                    //The registry key we need to check for SYSTEM PATH
                    //Get the value of the registry key
                    outPath = (string)Registry.LocalMachine.OpenSubKey(RegistryEditor.RegKeySystemPath).GetValue("Path", "", RegistryValueOptions.DoNotExpandEnvironmentNames);
                    break;
                case EnvironmentVariableTarget.User:
                    //The registry key we need to check for USER PATH
                    outPath = (string)Registry.CurrentUser.OpenSubKey(RegistryEditor.RegKeyUserPath).GetValue("PATH", "", RegistryValueOptions.DoNotExpandEnvironmentNames);
                    break;
                default:
                    throw new ArgumentException("Must use Machine or User target.");
            }

            //Send the result back to the caller, split on semicolons, and build into a List;
            return outPath.Split(';').ToList();

        }

        /// <summary>
        /// Writes a list of paths to the PATH environment variable in the registry. Starts an additional
        /// process if additional permissions are necessary.
        /// </summary>
        /// <param name="type">The USER or SYSTEM path.</param>
        /// <param name="path">A semi-color delimeted list of folder paths.</param>
        /// <param name="waitforprocess">Whether or not to wait for the process to finish. Optional. Defaults to true.</param>
        /// <remarks>
        /// When writing to the Local Machine registry hive, administrator privileges are required.
        /// This method will spawn an additional copy of this program as an administrator if writing
        /// to Local Machine.
        /// 
        /// You can decide to wait on this process or not by passing the optional third parameter.
        /// 
        /// If no administrator privileges are needed, it just writes to the registry.
        /// </remarks>
        public void WritePathsToRegistry(EnvironmentVariableTarget type, string path, bool waitforprocess = true) {

            switch (type) {
                case EnvironmentVariableTarget.Machine:
                    //Surround the path with quotes so we can pass it as an argument
                    string writePath = "\"" + path + "\"";
                                    
                    //Writing to the registry requires administrative privileges.
                    //Spawn a new copy of this exe with adminsitrative privileges
                    //so that the program can normally run with as little privileges
                    //as necessary.
                    ProcessStartInfo processInfo = new ProcessStartInfo();
                    processInfo.Verb = "runas";
                    processInfo.FileName = Application.ExecutablePath;
                    processInfo.Arguments = writePath;
                    Process p = Process.Start(processInfo);
                    if (waitforprocess) {
                        p.WaitForExit();
                    }

                    break;
                case EnvironmentVariableTarget.User:
                    //No admin privileges needed. Just write it.
                    Registry.CurrentUser.CreateSubKey(RegistryEditor.RegKeyUserPath).SetValue("Path", path, RegistryValueKind.ExpandString);

                    break;
                default:
                    throw new ArgumentException("Must use Machine or User target.");
            }


            
        }
    }
}
